import numpy as np
from itertools import product
from functools import reduce

class BayesianNetwork:
    def __init__(self, filename):
        f = open(filename, 'r') 
        N = int(f.readline())
        lines = f.readlines()

        self.tableList = []
        leafList = []

        for line in lines:
            node, parents, domain, shape, probabilities = self.__extract_model(line)
            leafList.append(node)
            for parent in parents:
                if parent in leafList:
                    leafList.remove(parent)

        for line in lines:
            node, parents, domain, shape, probabilities = self.__extract_model(line)

            parents.reverse()
            nodeDomain = [(node, domain)]
            for parent in parents:
                for customTable in self.tableList:
                    if parent in [*customTable.nodeList]:
                        nodeDomain.append((parent, [*customTable.nodeList[parent]]))
                        break

            if type(shape) is int:
                row = shape
            else:
                row = 1
                for dim in shape:
                    row = row * dim

            if len(parents) != 0:
                probabilities = np.reshape(probabilities, row)

            self.tableList.append(CustomTable([node], parents, node in leafList, nodeDomain, probabilities))

        f.close()

    def exact_inference(self, filename):
        result = 0
        f = open(filename, 'r')
        query_variables, evidence_variables = self.__extract_query(f.readline())
        # YOUR CODE HERE

        tableList = self.tableList.copy()

        queryList = [*query_variables]
        evidenceList = [*evidence_variables]
        hiddenList = []

        resultTable = None

        index = 0
        hiddenCheckList = []
        while index < len(tableList):
            table = tableList[index]
            if table.mainNode[0] not in hiddenCheckList:
                hiddenCheckList.append(table.mainNode[0])
                if (table.mainNode == queryList and table.parentNode == evidenceList):
                    resultTable = table
                    break
                for nodeName in queryList + evidenceList + hiddenList:
                    if table.isInvolve(nodeName):
                        mainNode = table.mainNode[0]
                        if mainNode in queryList or mainNode in evidenceList or mainNode in hiddenList:
                            for node in table.parentNode:
                                hiddenCheckList.remove(node)
                                if node not in hiddenList and node not in queryList and node not in evidenceList:
                                    hiddenList.append(node)
                        index = -1
                        break
            index += 1

        deletedTable = tableList.copy()
        for table in tableList:
            nodeName = table.mainNode[0]
            if nodeName not in queryList and nodeName not in evidenceList and nodeName not in hiddenList:
                deletedTable.remove(table)
        tableList = deletedTable
    
        if resultTable is None:
            for nodeName in hiddenList:
                tempTable = None
                index = 0
                while index < len(tableList):
                    table = tableList[index]
                    if table.isInvolve(nodeName):
                        tempTable = table if tempTable is None else tempTable.join(table)
                        tableList.remove(table)
                        index = 0
                    else:
                        index += 1
                if tempTable is not None:
                    tempTable = tempTable.eliminate(nodeName)
                    tableList.append(tempTable)

            for nodeName in queryList:
                index = 0
                while index < len(tableList):
                    table = tableList[index]
                    if table.isInvolve(nodeName):
                        resultTable = table if resultTable is None else resultTable.join(table)
                        tableList.remove(table)
                    else:
                        index += 1

            for nodeName in evidenceList:
                index = 0
                while index < len(tableList):
                    table = tableList[index]
                    if table.isInvolve(nodeName):
                        resultTable = table if resultTable is None else resultTable.join(table)
                        tableList.remove(table)
                    else:
                        index += 1

        normalizedNode = [node for node in resultTable.mainNode if node not in queryList]
        if len(normalizedNode) != 0:
            for node in normalizedNode:
                resultTable = resultTable.normalize(node)

        valueList = query_variables
        for name in evidenceList:
            valueList[name] = evidence_variables[name]

        result = resultTable.getValue(valueList)

        f.close()

        return result

    def __extract_model(self, line):
        parts = line.split(';')
        node = parts[0]
        if parts[1] == '':
            parents = []
        else:
            parents = parts[1].split(',')
        domain = parts[2].split(',')
        shape = eval(parts[3])
        probabilities = np.array(eval(parts[4])).reshape(shape)
        return node, parents, domain, shape, probabilities

    def __extract_query(self, line):
        parts = line.split(';')

        # extract query variables
        query_variables = {}
        for item in parts[0].split(','):
            if item is None or item == '':
                continue
            lst = item.split('=')
            query_variables[lst[0]] = lst[1]

        # extract evidence variables
        evidence_variables = {}
        for item in parts[1].split(','):
            if item is None or item == '':
                continue
            lst = item.split('=')
            evidence_variables[lst[0]] = lst[1]
        return query_variables, evidence_variables

class CustomTable:
    def __init__(self, mainNode, parentNode, isLeafNode, nodeDomain, table):
        self.mainNode = mainNode
        self.parentNode = parentNode
        self.isLeafNode = isLeafNode
        self.nodeList = {}
        self.row = len(table)
        self.table = table

        skipIndex = 1

        for domain in nodeDomain:
            subName, subDomain = domain
            domainList = {name:[] for name in subDomain}

            for subDomainIndex in range(len(subDomain)):
                if subDomainIndex == 0:
                    domainLength = round(self.row / len(subDomain))
                    tempList = [0] * domainLength
                    startIndex = subDomainIndex * skipIndex
                    domainIndex = 0
                    while domainIndex < domainLength:
                        for index in range(skipIndex):
                            tempList[domainIndex] = startIndex + index
                            domainIndex += 1
                        startIndex += skipIndex * len(subDomain)
                    domainList[subDomain[subDomainIndex]] = tempList
                else:
                    domainList[subDomain[subDomainIndex]] = [value + skipIndex for value in domainList[subDomain[subDomainIndex - 1]]]
                
            self.nodeList[subName] = domainList
            skipIndex = skipIndex * len(subDomain)

    def isInvolve(self, nodeName):
        return nodeName in self.mainNode or nodeName in self.parentNode

    def getValue(self, valueList):
        startIndex = 0
        endIndex = len(self.table) - 1

        nameList = [*self.nodeList]
        nameList.reverse()

        for name in nameList:
            tagList = [*self.nodeList[name]]
            tagCount = len(tagList)
            skipIndex = round((endIndex - startIndex + 1) / tagCount)
            tagIndex = tagList.index(valueList[name])
            startIndex = startIndex + (skipIndex * tagIndex)
            endIndex = endIndex - (skipIndex * (tagCount - tagIndex - 1))

        if (startIndex == endIndex):
            return self.table[startIndex]
        else:
            return -6.9

    def normalize(self, nodeName):
        newMainNode = self.mainNode.copy()
        newMainNode.remove(nodeName)

        newParentNode = self.parentNode.copy()
        newParentNode.append(nodeName)

        newNodeList = [(name, [*self.nodeList[name]]) for name in [*self.nodeList]]

        normalizeList = [nodeName] + self.parentNode.copy()

        newTable = [0] * self.row
        tagList = []
        for node in normalizeList:
            if len(tagList) == 0:
                for value in [*self.nodeList[node]]:
                    tagList.append([(node, value)])
            else:
                tempTagList = []
                for value in [*self.nodeList[node]]:
                    for curList in tagList:
                        tempTagList.append(curList + [(node, value)])
                tagList = tempTagList

        for tag in tagList:
            total = 0.0
            indexList = reduce(lambda x,y: set(x) & set(y), [self.nodeList[node[0]][node[1]] for node in tag])
            for listIndex in indexList:
                total += self.table[listIndex]
            for listIndex in indexList:
                newTable[listIndex] = self.table[listIndex] / total

        return CustomTable(newMainNode, newParentNode, False, newNodeList, newTable)

    def eliminate(self, nodeName):
        newMainNode = self.mainNode.copy()
        if nodeName in newMainNode:
            newMainNode.remove(nodeName)

        newParentNode = self.parentNode.copy()
        if nodeName in newParentNode:
            newParentNode.remove(nodeName)

        tempList = self.nodeList.copy()
        del tempList[nodeName]
        newSubNode = [(name, [*tempList[name]]) for name in [*tempList]]

        tableLength = 1
        for name in [*tempList]:
            tableLength = tableLength * len([*tempList[name]])
        newTable = [0 for x in range(tableLength)]
        indexList = list(self.nodeList[nodeName].values())
        for index in range(len(indexList[0])):
            for element in indexList:
                newTable[index] += self.table[element[index]]

        return CustomTable(newMainNode, newParentNode, False, newSubNode, newTable)

    def join(self, customTable):

        newMainNode = customTable.mainNode + self.mainNode

        newParentNode = []
        for node in customTable.parentNode:
            if node not in self.mainNode:
                newParentNode.append(node)
        for node in self.parentNode:
            if node not in customTable.mainNode and node not in newParentNode:
                newParentNode.append(node)

        newNodeList = []
        similarList = [node for node in [*customTable.nodeList] if node in [*self.nodeList]]
        for node in self.nodeList:
            if node not in similarList:
                newNodeList.append((node, [*self.nodeList[node]]))
        for node in customTable.nodeList:
            if node not in similarList:
                newNodeList.append((node, [*customTable.nodeList[node]]))
        for node in similarList:
            newNodeList.append((node, [*self.nodeList[node]]))

        totalNode = 1
        for node in newNodeList:
            totalNode *= len(node[1])

        if len(similarList) > 0:
            newTable = [0] * totalNode
            tagList = []
            for node in similarList:
                if len(tagList) == 0:
                    for value in [*self.nodeList[node]]:
                        tagList.append([(node, value)])
                else:
                    tempTagList = []
                    for value in [*self.nodeList[node]]:
                        for curList in tagList:
                            tempTagList.append(curList + [(node, value)])
                    tagList = tempTagList

            index = 0
            for tag in tagList:
                indexList1 = reduce(lambda x,y: set(x) & set(y), [self.nodeList[node[0]][node[1]] for node in tag])
                indexList2 = reduce(lambda x,y: set(x) & set(y), [customTable.nodeList[node[0]][node[1]] for node in tag])
                for index2 in indexList2:
                    for index1 in indexList1:
                        newTable[index] = self.table[index1] * customTable.table[index2]
                        index += 1
        else:
            newTable = [x * y for x, y in product(customTable.table, self.table)]

        return CustomTable(newMainNode, newParentNode, False, newNodeList, newTable)