# Bayesian Network

Constructing Bayesian Network and exact inferencing method.

- Command: python main.py --model="file model" --testcase="file test"
- Example: python main.py --model=model01.txt --testcase=testcase01.txt

Creator of this exercise: Nguyen Ho Man Rang
